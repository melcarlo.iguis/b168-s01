<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Activity</title>

	<h1>Full Address</h1>
	<p><?php echo getFullAddress('Philippines' , 'Metro Manila' , 'Quezon City' , '3F Caswynn Bldg., Timog Avenue');?></p>
	<p><?php echo getFullAddress('Philippines' , 'Aklan' , 'Malinao' , 'Purok uno Street, San Ramon');?></p>

	<h1>Letter-Based Grading</h1>
	<p>98 is equivalent to <?php echo getLetterGrade(98);?></p>
	<p>69 is equivalent to <?php echo getLetterGrade(69);?></p>
	<p>85 is equivalent to <?php echo getLetterGrade(85);?></p>
	<p>102 is equivalent to <?php echo getLetterGrade(102);?></p>
</head>
<body>
	
</body>
</html>
<?php 

//single line
/*multi line*/

// variable - use to store values or contain data
// variables in php are define using the dollar notation before the name of the variable

$name = 'John Smith';
$email = 'john@gmail.com';


// constant - are defined using the define()

define('PI', 3.1416);

// data types

// string
$state = 'New York';
$country = 'United States of America';
$address = $state.', '.$country; //concatenation via dot notation
$addressTwo = "$state, $country";

// integers

$age = 26;
$year = 2022;

// floats
$grade = 98.2;
$distanceInKm = 2453.23;

// boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;


$empty = null;


// Array
$grades = array(98.6, 4.2, 5,6);

// Objects
$gradesObj = (object)[
	'firstGrading' => 95.12,
	'secondGrading' => 92.1
];


$personObject = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' =>(object)[
			'state' => 'Washington DC',
			'country' => 'United state of america',
	]

];


// operators

$x = 56.2;
$y = 912.6;

$isLegalAge = true;
$isRegistered = false;

// functions

function getFullName($firstName , $middleInitial , $lastName){
	return "$lastName ,  $firstName $middleInitial.";
};


// Selection Control Structures

// if-elseif-else statement

function determineTyphoonIntensity($windspeed){
	if($windspeed < 30){
		return 'not a typhoon yet';
	}else if($windspeed <= 60){
		return 'Tropical depression detected';
	}else if($windspeed >= 62 && $windspeed <= 98){
		return 'Tropical storm detected';
	}else if($windspeed >= 89 && $windspeed <= 177){
		return 'Severe tropical storm detected';
	}else{
		return 'Typhoon detected';
	}
};


// Conditional Ternary Operator

function isUnderAge($age){
	return ($age < 18) ? true : false;
};


// Switch Statement
function determineComputerUser($computerNo){
	switch($computerNo){
		case 1:
			return 'Linus Torvals';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return 'Albert Einstein';
			break;
		case 5:
			return 'Charles Babbage';
			break;
		default:
			return $computerNo.' is out of bounds.';
	}
}


// try catch finally

function greeting($str){
	try{
		if(gettype($str) == "string"){
			echo $str;
		}else{
			throw new Exception("Ooops!");
		}
	}
	catch(Exception $e){
		echo $e->getMessage();
	}
	// finally - will run everytime even if the conditio fall to try or catch
	finally{
		echo " I did it again!";
	}

};